## Prowlarr
Prowlarr is an indexer manager/proxy built on the popular arr .net/reactjs base stack to integrate with your various PVR apps. Prowlarr supports management of both Torrent Trackers and Usenet Indexers. It integrates seamlessly with Lidarr, Mylar3, Radarr, Readarr, and Sonarr offering complete management of your indexers with no per app Indexer setup required (we do it all).

## Bootstrap
```shell
bastille bootstrap https://gitlab.com/bastillebsd-templates/prowlarr
```

## Usage
```shell
bastille template TARGET bastillebsd-templates/prowlarr
```

### Available arguments `--arg NAME=value`
- USER=prowlarr
- GROUP=prowlarr
- DATA_DIR=/usr/local/prowlarr
- OUT_PORT=9696

For more options edit `/usr/local/bastille/templates/bastillebsd-templates/Prowlarr/Bastillefile`